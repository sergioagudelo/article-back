export type SpaceflightResponse = {
  events?: Array<any>;
  featured?: boolean;
  id?: number;
  imageUrl: string;
  launches?: Array<any>;
  newsSite?: string;
  publishedAt?: string;
  summary?: string;
  title: string;
  updatedAt?: string;
  url: string;
};
