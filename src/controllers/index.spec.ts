import { filterResponse, cacheResponse } from './index';
import { data } from './index.data'

describe('index controller', () => {
    it('filterResponse', () => {
        expect(filterResponse(data)).toEqual(data);
    });
});