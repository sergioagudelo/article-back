import { Request, Response } from 'express';
import axios from 'axios';

import NodeCache from 'node-cache';
import { SpaceflightResponse } from '../types/SpaceflightResponse';


const cache = new NodeCache({
    checkperiod: 60,
    maxKeys: 10000,
    stdTTL: 300,
    useClones: false,
});

export const cacheResponse = async (req: Request, res: Response): Promise<Response> => {
    try {
        const cachedResponse = cache.get('cachedResponse');
        if (cachedResponse) {
            return res.status(200).json(cachedResponse);
        }

        const response = await axios({
            url: 'https://api.spaceflightnewsapi.net/v3/articles?_limit=100',
            method: 'get',
        });

        const filteredResponse = filterResponse(response.data)

        cache.set('cachedResponse', filteredResponse);
        return res.status(200).json(filteredResponse);

    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
};

export const filterResponse = (response: SpaceflightResponse[]) => response.map((res: SpaceflightResponse) => {
    return { imageUrl: res.imageUrl, url: res.url, title: res.title }
});
