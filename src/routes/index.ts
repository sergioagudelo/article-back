import { Router } from 'express';
const router = Router();

import { cacheResponse } from '../controllers/index';

router.get('/cache', cacheResponse);

export default router;
